# Developers corner

This section covers everything related to developing GUA.

It is recommended to use VSCode with Golang plugin as code editor as it does majority of neccessary tasks (like linting, formatting, etc.) automagically.

* [Code Style](/doc/developers/codestyle.md) - how to write code properly.