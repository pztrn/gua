# Code Style

While developing GUA you should:

## Use linters and follow their suggestions

Linting is a very important process because it helps to get rid of child mistakes like "I forgot to describe function in comment", "I've initialized variable and forgot to use it, compilation failed" and such. Don't make everyone angry on you because of such errors.

## Use formatters

We follow [official Golang recommendations about code styling](https://github.com/golang/go/wiki/CodeReviewComments) which presumes that you'll use formatters like gofmt. Don't using formatters will result in your MR closed with resolution "unformatted shit".

## Do not fit code lines in 72-80 chars

This comes from past and should be eliminated. Don't do this. Forget about silly things like "typography", because if function call takes 30 lines on screen instead of 3 - it will be uncomfortable to read.

## Comments should be 72-80 chars long

This rule helps to delimit code blocks from each other with comments block visually.

## Comments should be useful

Try to explain as much as possible about your code, especially if you're doing hard to understand math or bitwise operations, because not all developers are magisters in math or computer science. Code should be easily understandable by bearded 30 years old math doctor who writes cross-platform application for geodesian things and 15 years old kid who want to write a Minecraft launcher. But don't overcomment your code - it is useless to place a comment near ``for`` loop that says "Iterating" when you're just iterating some strings from slice. Yet it'll be useful to explain your iteration of widgets slice and what it actually do.