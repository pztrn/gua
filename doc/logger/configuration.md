# Logger's configuration

Logger can be configured using environment variables.

## Logging level

* Environment variable: ``GUA_LOGGER_LEVEL``
* Possible values: ``DEBUG``, ``INFO``, ``WARN``, ``ERROR``, ``FATAL``, ``PANIC``
* Default: ``INFO``

Defines logging level for whole application.

Example:

```bash
GUA_LOGGING_LEVEL=WARN go run main.go
```

## Show application's memory usage

* Environment variable: ``GUA_LOGGER_SHOW_MEMORY_USAGE``
* Possible values: ``1`` or ``0``
* Default: ``0``

Show application's memory usage in logs. Parameters that will be shown:

1. Memory allocation (``memalloc`` field)
2. Memory taken from OS (``memsys`` field)
3. GC runs (``numgc`` field)

## Show caller's data

* Environment variable: ``GUA_LOGGER_SHOW_CALLER``
* Possible values: ``1`` or ``0``
* Default: ``0``

Shows caller's information in every log line.

Example:

```bash
GUA_LOGGER_SHOW_CALLER=1 go run main.go
```

Example output:

```
2019-02-18T09:40:19+05:00 | INFO   | Initializing context... module=context
2019-02-18T09:40:19+05:00 | INFO   | Starting mainwindow example... called by=main.go:12:main.main memalloc=0MB memsys=66MB numgc=0
2019-02-18T09:40:19+05:00 | DEBUG  | GUI module initialized called by=gitlab.com/pztrn/gua/exported.go:14:gui.Initialize memalloc=0MB memsys=66MB numgc=0
```