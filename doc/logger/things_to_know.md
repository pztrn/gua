# Things to know

This page contains some things that should be noted by every developer.

## Fields that should not be set

Right now we're using ZeroLog as logging facitily, and it have some limitations in fields setting:

* Do not set field ``level`` manually, it affects log message level.
* Do not set field ``caller`` manually, it breaks log message formatting.