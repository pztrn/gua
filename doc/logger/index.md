# Logger

This page contains documentation about logging facility provided by GUA.

## What's inside

GUA uses [ZeroLog](https://github.com/rs/zerolog) as default logging facility at this moment, so everything described in official documentation can be used with context's Logger.

There are plans to make usage of other logger possible, but it's not a high priority task, so MRs are very welcome.

One thing to mention - create new loggers from context's Logger to ensure that it will be configured in same way and will use same hooks.

* [Configuration](/doc/logger/configuration.md) - how to configure logger.
* [Things to know](/doc/logger/things_to_know.md) - some things that should be noted.