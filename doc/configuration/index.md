# Configuration

This page contains documentation about configuration facility provided by GUA.

## What's inside

By default GUA provides SQLite storage (from Database module) which stores persistent configuration in table called "configuration".

More backends and configuration implementations might follow.

## Tips

### What to use for configuration keys

As configuration storage is a key-value one, try to use something like "/SYSTEM/KEY", like we do for windows size saving (it's something like ``/mainwindow/size_y``).