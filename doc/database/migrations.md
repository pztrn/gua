# Database migrations

Every developer sooner or later encounter a problem with database migrations. This topic became especially important if you're using something like GUA and do not want to recreate a bicycle. To eliminate all possible problems with third-party migrations tools GUA provides pretty simple mechanism to deal with migrations.

## How it works

As Database structure provides a way to work with databases using [SQLX library](https://github.com/jmoiron/sqlx), it's good to re-use it for migrations without third-party tools that might complicate things. Example of complication - trying to use [Goose](https://github.com/pressly/goose) due to usage of global variables and inability to easily create separate migration sequences for different databases. As we're going to add more backends soon - we should create own bicycle.

What should you know while creating your migrations:

1. Use real migration creation datestamps, as it will be used as sorting key for whole sequence. This is a must, because GUA might use own migrations, e.g. for "configuration" table, or for any other system table and they should be applied in right sequence.
2. Use meaningful function names for migrations. This is opt-in requirement, but will help in debug alot.
3. Do not use structs while working with migrations, if you're not doing structs versioning, as this will lead to inability to compile.