# Database

This page contains information about database access available via GUA.

## For what it is used or might be used

Currently GUA uses SQLite3 database for storing settings.

Database might be used for caching data, for example. SQLite3 known to work pretty well on near-terabyte databases.

## Docs

There are some topics that covered by subdocuments:

* [Migrations](/doc/database/migrations.md) - how to create and execute migrations.