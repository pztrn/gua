# GUA

Wecome to GUA documentation.

## What is GUA?

**GUA** stands for **GoTK3 Useful Additions**, it contains everything any modern GUI application is needed and should make developer's life easier.

GUA provides:

* Local application's database (sqlite3) for storing data locally.
* Configuration manager based on local database.
* Easy constructing and working with main window, menus, etc.
* Logging facility (github.com/rs/zerolog) with possibility to show file name and line where event occured, for easy debugging.
* Event manager to write completely asynchronous event-driven applications.

## For developers

Take a look at [developers corner](/doc/developers/index.md) of documentation.

## The Documentation

* [Getting started](/doc/getting_started/index.md) - how to start using GUA.

### Modules

* [Database](/doc/database/index.md) - everything about local database access.
* [Logger](/doc/logger/index.md) - everything about logging facility provided by GUA.

//Documentation will be extended in future.//