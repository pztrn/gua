# GUA

**GUA** is a framework for writing GUI applications in Go.

It contains a complete set of utilities and helpers that should make your life much easier.

For a complete and up-to-date documentation take a look at [doc directory](/doc/index.md).

## Version things

Until v1.0.0 there is a possibility of breaking changes to API. Use dependency managers to workaround that and pin exact revision or version of GUA to your application.

Go modules won't be supported in nearest future.

## Under the hood

ATM GUA uses GTK3 for all platforms. This might change in future.

## The GNOME HIG

GUA does not respect GNOME HIG as well as does not enforce it. You might follow it (or not).

## Donate

Donate to keep project alive! You can do this:

* [Via PayPal](https://www.paypal.me/pztrn)
