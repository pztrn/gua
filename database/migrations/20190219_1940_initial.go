package migrations

import (
	// other
	"github.com/jmoiron/sqlx"
)

// InitialGUAMigrationUp creates initial database structure used by GUA.
func InitialGUAMigrationUp(tx *sqlx.Tx) error {
	if _, err := tx.Exec(`
DROP TABLE IF EXISTS database_version;
CREATE TABLE database_version (
	version         VARCHAR(1024)   NOT NULL,
	action			INT(1)			NOT NULL,
	timestamp		timestamp		NOT NULL
);

DROP TABLE IF EXISTS configuration;
CREATE TABLE configuration (
	key VARCHAR(128) NOT NULL,
	value VARCHAR(8192) NOT NULL
);
	`); err != nil {
		return err
	}

	return nil
}

// InitialGUAMigrationDown rollbacks initial database structure.
func InitialGUAMigrationDown(tx *sqlx.Tx) error {
	if _, err := tx.Exec(`
DROP TABLE database_version;
DROP TABLE configuration;
	`); err != nil {
		return err
	}

	return nil
}
