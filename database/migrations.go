package database

import (
	// stdlib
	"sort"
	"time"

	// local
	"gitlab.com/pztrn/gua/database/migrations"
	"gitlab.com/pztrn/gua/database/models"

	// other
	"github.com/jmoiron/sqlx"
)

const (
	// MigrationApplyAction shows that migration was applied.
	MigrationApplyAction MigrationAction = 0
	// MigrationRollbackAction shows that migration was rolled back.
	MigrationRollbackAction MigrationAction = 1
)

var (
	registeredMigrations map[string]*Migration
)

// Migration represents a single migration.
type Migration struct {
	// Name is a timestamp when migration was created. Here should go
	// a timestamp in "YYYY-mm-dd HH:MM:SS" format.
	Name string
	// Description is a migration description. It will be printed
	// out.
	Description string
	// UpFunc is a function that will apply migration.
	UpFunc MigrationFunc
	// DownFunc is a function that will rollback migration.
	DownFunc MigrationFunc
}

// MigrationFunc represents function signature for UpFunc and DownFunc
// migrations functions.
type MigrationFunc func(tx *sqlx.Tx) error

// MigrationAction
type MigrationAction int

// AddMigration registers migration for execution.
func AddMigration(migration *Migration) bool {
	if registeredMigrations == nil {
		registeredMigrations = make(map[string]*Migration)
	}

	_, alreadyRegistered := registeredMigrations[migration.Name]
	if alreadyRegistered {
		logger.Error().Str("migration name", migration.Name).Msg("Migration already registered")
		return false
	}

	registeredMigrations[migration.Name] = migration
	logger.Debug().Str("migration name", migration.Name).Str("migration description", migration.Description).Msg("Registered migration")
	return true
}

func migrate(db *sqlx.DB) {
	logger.Info().Msg("Migrating local database...")

	// Getting current database version.
	appliedMigrations := []*models.DatabaseVersionEntry{}
	err := db.Select(&appliedMigrations, "SELECT * FROM database_version ORDER BY timestamp ASC")
	if err != nil {
		// We doesn't care about this error actually, because it shows
		// that we're in the very beginning.
		if err.Error() != "no such table: database_version" {
			logger.Fatal().Err(err).Msg("Got error while checking database version")
		} else {
			logger.Debug().Msg("No database structure found, will create new one")
		}
	}
	migrationsToSkip := 0
	for _, appliedMigration := range appliedMigrations {
		if appliedMigration.Action == int(MigrationApplyAction) {
			migrationsToSkip++
		} else if appliedMigration.Action == int(MigrationRollbackAction) {
			migrationsToSkip--
		} else {
			logger.Fatal().Int("action", appliedMigration.Action).Msg("Failed to calculate database version, unknown action stored in database")
		}
	}

	// Get sorted slice of registered migrations.
	var registeredMigrationsKeys []string
	for n := range registeredMigrations {
		registeredMigrationsKeys = append(registeredMigrationsKeys, n)
	}
	sort.Strings(registeredMigrationsKeys)

	var failed bool
	// Start database transaction and execute migrations.
	tx, err := db.Beginx()
	if err != nil {
		logger.Fatal().Err(err).Msg("Failed to start database transaction for applying migrations")
	}
	// Figure out what we should really do - apply or rollback.
	whatToDo, _ := c.Flagger.GetStringValue("DBMigrateAction")
	if whatToDo == "up" {
		// Copy only needed migrations for applying.
		migrationsForApply := registeredMigrationsKeys[migrationsToSkip:]
		logger.Debug().Msgf("Migrations to apply: %+v", migrationsForApply)

		for _, name := range migrationsForApply {
			logger.Debug().Str("migration name", name).Msg("Applying migration")
			err := registeredMigrations[name].UpFunc(tx)
			if err != nil {
				failed = true
				break
			}
			tx.MustExec("INSERT INTO database_version (version, action, timestamp) VALUES (?, ?, ?)", name, MigrationApplyAction, time.Now().UTC())
		}
	} else if whatToDo == "down" {
		count, _ := c.Flagger.GetIntValue("DBMigrateCount")
		// We know applied migrations count and migrations count we
		// should rollback. We should reverse registeredMigrationsKeys
		// slice and take only migrations we needed.
		var reversedKeys []string

		// >-1 because i is a slice index.
		for i := len(registeredMigrationsKeys) - 1; i > -1; i-- {
			reversedKeys = append(reversedKeys, registeredMigrationsKeys[i])
		}

		// Take what we need.
		migrationsToRollback := reversedKeys
		if count != 0 {
			migrationsToRollback = reversedKeys[:count]

		}
		logger.Debug().Msgf("Migrations to rollback: %+v", migrationsToRollback)
		for _, name := range migrationsToRollback {
			logger.Debug().Str("migration name", name).Msg("Rolling back migration")
			err := registeredMigrations[name].DownFunc(tx)
			if err != nil {
				failed = true
				break
			}
			// We might want to execute complete rollback (when
			// DBMigrateAction is "down" and DBMigrateCount is 0 or
			// equal to whole migrations count). In that case we should
			// not write to database if we have rolled back initial
			// migration named "0".
			if name != "0" {
				tx.MustExec("INSERT INTO database_version (version, action, timestamp) VALUES (?, ?, ?)", name, MigrationRollbackAction, time.Now().UTC())
			}
		}

	} else {
		logger.Fatal().Msg("Action is not one of 'up' or 'down'. If you're seeing this message - than this is a BUG and should be reported to developers!")
	}

	if failed {
		tx.Rollback()
	} else {
		tx.Commit()
	}
}

func registerMigrations() {
	AddMigration(&Migration{
		Name:        "0",
		Description: "Initial GUA migration",
		UpFunc:      migrations.InitialGUAMigrationUp,
		DownFunc:    migrations.InitialGUAMigrationDown,
	})
}
