package database

import (
	// stdlib
	"path/filepath"

	// local
	"gitlab.com/pztrn/gua/context"
	"gitlab.com/pztrn/gua/database/interface"
	"gitlab.com/pztrn/gua/eventer/handler"

	// other
	"github.com/jmoiron/sqlx"
	// sqlite backend
	_ "github.com/mattn/go-sqlite3"
	"github.com/rs/zerolog"
	"gitlab.com/pztrn/flagger"
)

var (
	c      *context.Context
	d      *Database
	logger zerolog.Logger
)

// NewDatabase creates new Database struct, registers it with context
// and add some event handlers to Eventer to ensure proper initialization.
func NewDatabase(cc *context.Context) {
	c = cc
	logger = c.Logger.With().Str("GUA module", "database").Logger()
	logger.Debug().Msg("Initializing local database provider...")
	d = &Database{}
	c.RegisterDatabaseInterface(databaseinterface.Interface(Handler{}))
	registerMigrations()

	if c.IsEventerRegistered() {
		c.Eventer.AddEventHandler("startup", 11, eventhandler.EventHandler{
			Name:    "initialize local database",
			Handler: d.Initialize,
		})

		c.Eventer.AddEventHandler("shutdown", 99997, eventhandler.EventHandler{
			Name:    "shutdown local database",
			Handler: d.Shutdown,
		})
	}

	c.Flagger.AddFlag(&flagger.Flag{
		Name:         "DBMigrateAction",
		Description:  "Database migrationg action. Possible values: up, down. Default - up.",
		Type:         "string",
		DefaultValue: "up",
	})

	c.Flagger.AddFlag(&flagger.Flag{
		Name:         "DBMigrateCount",
		Description:  "How many migrations apply or rollback. 0 - all. Default - 0.",
		Type:         "int",
		DefaultValue: 0,
	})

	c.Flagger.AddFlag(&flagger.Flag{
		Name:         "DBMigrateOnly",
		Description:  "Only perform database migration and immediately exit.",
		Type:         "bool",
		DefaultValue: false,
	})
	logger.Debug().Msg("Local database provider initialized.")
}

// Database provides SQLite database.
type Database struct {
	databasePath string
	db           *sqlx.DB
}

// GetConnection returns connection pointer to database.
func (db *Database) GetConnection() *sqlx.DB {
	return db.db
}

// Initializes Database structure, connections and migrates database.
func (db *Database) Initialize(data map[interface{}]interface{}) (bool, error) {
	logger.Info().Msg("Initializing local database...")

	db.initializeDatabasePaths()
	db.initializeConnection()

	if db.db != nil {
		migrate(db.db)
	}

	return true, nil
}

// Initializes connection to database.
func (db *Database) initializeConnection() {
	logger.Debug().Str("database path", db.databasePath).Msg("Opening connection to database...")

	dbConn, err := sqlx.Connect("sqlite3", db.databasePath+"?parseTime=true")
	if err != nil {
		logger.Fatal().Err(err).Msg("Failed to connect to database")
	}

	logger.Debug().Msg("Database connection established")
	db.db = dbConn
}

// Initializes database path.
func (db *Database) initializeDatabasePaths() {
	logger.Debug().Msg("Figuring out where to keep our database...")

	datapath, _ := c.Configurator.GetTempValue("/paths/data")
	db.databasePath = filepath.Join(datapath.(string), "database.sqlite3")

	logger.Debug().Str("database path", db.databasePath).Msg("Database path calculated")

}

// Shutdown is an event handler for "shutdown" event and should not be
// called manually, unless Eventer isn't in use.
func (db *Database) Shutdown(data map[interface{}]interface{}) (bool, error) {
	logger.Info().Msg("Shutting down database connection...")

	db.db.Close()

	return true, nil
}
