package databaseinterface

import (
	// other
	"github.com/jmoiron/sqlx"
)

type Interface interface {
	GetConnection() *sqlx.DB
}
