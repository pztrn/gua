package models

type Configuration struct {
	Key   string `db:"key"`
	Value string `db:"value"`
}
