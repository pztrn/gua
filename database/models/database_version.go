package models

import (
	// stdlib
	"time"
)

// DatabaseVersionEntry represents a single entry in databae about
// applied or rolled back migrations.
type DatabaseVersionEntry struct {
	// Version is a migration name in format "YYYY-mm-dd HH:MM:SS".
	Version string `db:"version"`
	// Action is a Migration*Action flag from database package.
	Action int `db:"action"`
	// Timestamp is a timestamp when this action was performed, in UTC.
	Timestamp time.Time `db:"timestamp"`
}
