package eventer

import (
	// stdlib
	"sort"

	// local
	"github.com/rs/zerolog"
	"gitlab.com/pztrn/gua/context"
	"gitlab.com/pztrn/gua/eventer/handler"
	"gitlab.com/pztrn/gua/eventer/interface"
)

var (
	c  *context.Context
	ev *Eventer
)

// NewEventer initializes Eventer structure.
func NewEventer(cc *context.Context) {
	c = cc
	ev = &Eventer{}
	ev.Initialize()
	c.RegisterEventerInterface(eventerinterface.Interface(Handler{}))
}

// Eventer provides event handling features.
// Events are registered automatically on adding first event handler.
type Eventer struct {
	events map[string]map[int]eventhandler.EventHandler

	logger zerolog.Logger
}

// AddEventHandler registers event handler for event with defined execution step.
func (e *Eventer) AddEventHandler(name string, actionNumber int, handler eventhandler.EventHandler) bool {
	if name == "" {
		e.logger.Error().Msg("Cannot create event handler queue with empty event name!")
		return false
	}

	_, eventAlreadyCreated := e.events[name]
	if !eventAlreadyCreated {
		e.events[name] = make(map[int]eventhandler.EventHandler)
	}

	e.events[name][actionNumber] = handler
	e.logger.Debug().Str("handler name", handler.Name).Int("execution step", actionNumber).Str("queue", name).Msg("Added handler to queue with execution number")

	return true
}

// Initialize initializes Eventer.
func (e *Eventer) Initialize() {
	e.logger = c.Logger.With().Str("GUA module", "eventer").Logger()

	e.logger.Debug().Msg("Initializing events handler...")
	e.events = make(map[string]map[int]eventhandler.EventHandler)
}

// LaunchEvent launches handlers for specified event and passing them
// passed data map.
func (e *Eventer) LaunchEvent(name string, data map[interface{}]interface{}) bool {
	handlers, handlersPresent := e.events[name]
	if !handlersPresent {
		e.logger.Error().Str("event name", name).Msg("Handlers for event wasn't registered")
		return false
	}

	e.logger.Debug().Str("event name", name).Msg("Launching event...")

	var handlersNums []int
	for num := range handlers {
		handlersNums = append(handlersNums, num)
	}

	sort.Ints(handlersNums)

	for _, num := range handlersNums {
		e.logger.Debug().Str("handler name", handlers[num].Name).Str("queue", name).Int("execution number", num).Msg("Launching handler from queue with execution number")

		if len(handlers[num].AdditionalData) > 0 {
			if data == nil {
				data = make(map[interface{}]interface{})
			}

			for k, v := range handlers[num].AdditionalData {
				data[k] = v
			}
		}

		success, err := handlers[num].Handler(data)
		if !success {
			e.logger.Error().Err(err).Str("event name", name).Int("execution number", num).Msg("Error occured for event")
			return false
		}
	}

	return true
}
