package eventer

import (
	// local
	"gitlab.com/pztrn/gua/eventer/handler"
)

type Handler struct{}

func (eh Handler) AddEventHandler(name string, actionNumber int, handler eventhandler.EventHandler) bool {
	return ev.AddEventHandler(name, actionNumber, handler)
}

func (eh Handler) LaunchEvent(name string, data map[interface{}]interface{}) bool {
	return ev.LaunchEvent(name, data)
}
