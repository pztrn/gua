package eventhandler

// EventHandler is a structure that holds a handler for specific event
// and it's medatata.
type EventHandler struct {
	// Name is an event handler name.
	Name string
	// EventName is an event name. Will be set on event handler registration.
	EventName string
	// Handler is a function that handling neccessary event.
	Handler func(data map[interface{}]interface{}) (bool, error)
	// AdditionalData is a map with data that should be passed to handler
	// on every call.
	AdditionalData map[interface{}]interface{}
}
