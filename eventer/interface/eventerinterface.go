package eventerinterface

import (
	// local
	"gitlab.com/pztrn/gua/eventer/handler"
)

// Interface is a Eventer interface that is registered with Context.
type Interface interface {
	AddEventHandler(name string, actionNumber int, handler eventhandler.EventHandler) bool
	LaunchEvent(name string, data map[interface{}]interface{}) bool
}
