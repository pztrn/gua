package context

import (
	// stdlib
	"sync"

	// local
	"gitlab.com/pztrn/gua/configurator/interface"
	"gitlab.com/pztrn/gua/database/interface"
	"gitlab.com/pztrn/gua/eventer/interface"

	// other
	"github.com/gotk3/gotk3/gtk"
	"github.com/rs/zerolog"
	"gitlab.com/pztrn/flagger"
)

func New() *Context {
	return &Context{}
}

type Context struct {
	Application  *gtk.Application
	Configurator configuratorinterface.Interface
	Database     databaseinterface.Interface
	Eventer      eventerinterface.Interface
	Flagger      *flagger.Flagger
	Logger       zerolog.Logger

	// Interface registration flags.
	eventerRegistered      bool
	eventerRegisteredMutex sync.Mutex

	// Application's data.
	appName    string
	appID      string
	appVersion string
}

// InitializeGUAContextPre initializes GUA context at application's launch.
func (c *Context) InitializeGUAContextPre() {
	c.initializeLoggerPre()

	c.Flagger = flagger.New(c.appID, nil)
	c.Flagger.Initialize()
}

// InitializeGUAContextPost executes GUA context initialization actions
// that should be launched after CLI flags parsing.
func (c *Context) InitializeGUAContextPost() {
}

// IsEventerRegistered returns boolean value that indicates if Eventer
// was previously registered.
func (c *Context) IsEventerRegistered() bool {
	c.eventerRegisteredMutex.Lock()
	defer c.eventerRegisteredMutex.Unlock()
	return c.eventerRegistered
}

// RegisterConfiguratorInterface register configuration worker interface.
// Should not be called manually unless you want to perform
// some dark magic.
func (c *Context) RegisterConfiguratorInterface(ci configuratorinterface.Interface) {
	c.Configurator = ci
}

// RegisterDatabaseInterface registers database handling interface.
// Should not be called manually unless you want to perform
// some dark magic.
func (c *Context) RegisterDatabaseInterface(di databaseinterface.Interface) {
	c.Database = di
}

// RegisterEventerInterface registers Eventer interface for custom events
// handling. Should not be called manually unless you want to perform
// some dark magic.
func (c *Context) RegisterEventerInterface(ei eventerinterface.Interface) {
	c.Eventer = ei
	c.eventerRegisteredMutex.Lock()
	c.eventerRegistered = true
	c.eventerRegisteredMutex.Unlock()
}

// SetApplicationData sets neccessary application data.
// appID will be used while constructing GTK's Application.
// appName will be used everywhere wher it is needed, like window title.
// appVersion can be used everywhere in application.
func (c *Context) SetApplicationData(appID string, appName string, appVersion string) {
	c.appID = appID
	c.appName = appName
	c.appVersion = appVersion
}
