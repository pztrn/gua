package context

import (
	// stdlib
	"fmt"
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"
	"time"

	// other
	"github.com/rs/zerolog"
)

const (
	// How many frames we should skip on every getCaller launch apart
	// added hooks.
	skipFrames = 3
)

var (
	// How many hooks was added in runtime?
	hooksAdded = 0
)

// AddLoggerHook adds hook to logger.
func (c *Context) AddLoggerHook(hook func(e *zerolog.Event, level zerolog.Level, message string)) {
	c.Logger = c.Logger.Hook(zerolog.HookFunc(hook))
	hooksAdded++
}

// Caller information in logger.
// Probably will break if another hook is added?
func (c *Context) getCaller(e *zerolog.Event, level zerolog.Level, message string) {
	frame := c.getFrame(skipFrames + hooksAdded)
	filePathRaw, line := frame.Func.FileLine(frame.PC)
	_, fileName := path.Split(filePathRaw)
	dirName, funcName := path.Split(frame.Func.Name())
	e.Str("called by", dirName+fileName+":"+strconv.Itoa(line)+":"+funcName)
}

// Get calling frames.
func (с *Context) getFrame(skipFrames int) runtime.Frame {
	// We need the frame at index skipFrames+2, since we never want runtime.Callers and getFrame
	targetFrameIndex := skipFrames + 2

	// Set size to targetFrameIndex+2 to ensure we have room for one more caller than we need
	programCounters := make([]uintptr, targetFrameIndex+2)
	n := runtime.Callers(0, programCounters)

	frame := runtime.Frame{Function: "unknown", Line: 0, File: "unknown"}
	if n > 0 {
		frames := runtime.CallersFrames(programCounters[:n])
		for more, frameIndex := true, 0; more && frameIndex <= targetFrameIndex; frameIndex++ {
			var frameCandidate runtime.Frame
			frameCandidate, more = frames.Next()
			if frameIndex == targetFrameIndex {
				frame = frameCandidate
			}
		}
	}

	return frame
}

// Memory usage in logging output.
func (c *Context) getMemoryUsage(e *zerolog.Event, level zerolog.Level, message string) {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	e.Str("memalloc", fmt.Sprintf("%dMB", m.Alloc/1024/1024))
	e.Str("memsys", fmt.Sprintf("%dMB", m.Sys/1024/1024))
	e.Str("numgc", fmt.Sprintf("%d", m.NumGC))
}

// Initializes basic logger with enabled DEBUG level.
// This function executes on early context initialization, logging
// level and other things can be overrided later.
func (c *Context) initializeLoggerPre() {
	// Logging formatting.
	output := zerolog.ConsoleWriter{Out: os.Stdout, NoColor: false, TimeFormat: time.RFC3339}
	output.FormatLevel = func(i interface{}) string {
		var v string
		if ii, ok := i.(string); ok {
			ii = strings.ToUpper(ii)
			switch ii {
			case "DEBUG":
				v = fmt.Sprintf("\x1b[30m%-6s\x1b[0m", ii)
			case "ERROR":
				v = fmt.Sprintf("\x1b[31m%-6s\x1b[0m", ii)
			case "FATAL":
				v = fmt.Sprintf("\x1b[35m%-6s\x1b[0m", ii)
			case "INFO":
				v = fmt.Sprintf("\x1b[32m%-6s\x1b[0m", ii)
			case "PANIC":
				v = fmt.Sprintf("\x1b[36m%-6s\x1b[0m", ii)
			case "WARN":
				v = fmt.Sprintf("\x1b[33m%-6s\x1b[0m", ii)
			default:
				v = ii
			}
		}
		return fmt.Sprintf("| %s |", v)
	}

	c.Logger = zerolog.New(output).With().Timestamp().Logger()
	c.Logger.Info().Str("module", "context").Msg("Initializing context...")

	// Add memory usage printing to logger only if GUA_LOGGER_SHOW_MEMORY_USAGE
	// if defined.
	_, showMemUsageFlagFound := os.LookupEnv("GUA_LOGGER_SHOW_MEMORY_USAGE")
	if showMemUsageFlagFound {
		c.AddLoggerHook(c.getMemoryUsage)
	}
	// Add caller's file name and lineno only if GUA_SHOW_CALLER_IN_LOG
	// is defined.
	_, showCallerInLogFlagFound := os.LookupEnv("GUA_LOGGER_SHOW_CALLER")
	if showCallerInLogFlagFound {
		c.AddLoggerHook(c.getCaller)
	}

	c.initializeLoggerLevel()
}

// Initializes logger depending on configuration settings and environment
// variables values.
// Note: environment variables takes precedence over configuration values,
// because configuration might be broken.
func (c *Context) initializeLoggerLevel() {
	// Logging level.
	// ToDo: get logging level from configuration.
	var loglevel string
	loglevelFromEnv, found := os.LookupEnv("GUA_LOGGER_LEVEL")
	if found {
		loglevel = loglevelFromEnv
	} else {
		loglevel = "INFO"
	}

	c.Logger.Debug().Str("logging level to set", loglevel).Msg("Setting logging level")

	switch loglevel {
	case "DEBUG":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "INFO":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "WARN":
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case "ERROR":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	case "FATAL":
		zerolog.SetGlobalLevel(zerolog.FatalLevel)
	}
}
