package main

import (
	// local
	"gitlab.com/pztrn/gua/configurator"
	"gitlab.com/pztrn/gua/eventer"

	// other
	"gitlab.com/pztrn/gua/context"
	"gitlab.com/pztrn/gua/database"
	"gitlab.com/pztrn/gua/gui"
)

func main() {
	c := context.New()
	c.SetApplicationData("name.pztrn.mainwindow_example", "MainWindow Example", "0.1.0")
	c.InitializeGUAContextPre()
	c.Logger.Info().Msg("Starting mainwindow example...")

	eventer.NewEventer(c)
	configurator.NewConfigurator(c)
	database.NewDatabase(c)

	gui.Initialize(c)

	c.Flagger.Parse()

	c.Eventer.LaunchEvent("startup", nil)
}
