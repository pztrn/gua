package configurator

type Handler struct{}

func (cfgh Handler) GetTempValue(key string) (interface{}, error) {
	return conf.GetTempValue(key)
}

func (cfgh Handler) GetValue(key string) (string, error) {
	return conf.GetValue(key)
}

func (cfgh Handler) SetTempValue(key, value string) {
	conf.SetTempValue(key, value)
}

func (cfgh Handler) SetValue(key, value string) {
	conf.SetValue(key, value)
}
