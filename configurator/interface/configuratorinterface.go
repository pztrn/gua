package configuratorinterface

// Interface is a Configurator interface that is registered with Context.
type Interface interface {
	GetTempValue(key string) (interface{}, error)
	GetValue(key string) (string, error)
	SetTempValue(key, value string)
	SetValue(key, value string)
}
