package configurator

import (
	// stdlib
	"errors"
	"os"
	"path/filepath"
	"runtime"
	"sync"

	// local
	"gitlab.com/pztrn/gua/configurator/interface"
	"gitlab.com/pztrn/gua/context"
	"gitlab.com/pztrn/gua/database/models"
	"gitlab.com/pztrn/gua/eventer/handler"

	// other
	"github.com/rs/zerolog"
)

var (
	c    *context.Context
	conf *Configurator
)

// NewConfigurator creates empty Configurator struct, registers
// it with Context and registers some event handlers for startup and
// shutdown.
func NewConfigurator(cc *context.Context) {
	c = cc
	conf = &Configurator{}
	c.RegisterConfiguratorInterface(configuratorinterface.Interface(Handler{}))

	if c.IsEventerRegistered() {
		c.Eventer.AddEventHandler("startup", 10, eventhandler.EventHandler{
			Name:    "initialize configuration storage",
			Handler: conf.Initialize,
		})

		c.Eventer.AddEventHandler("startup", 60, eventhandler.EventHandler{
			Name:    "load configuration",
			Handler: conf.LoadConfiguration,
		})

		c.Eventer.AddEventHandler("shutdown", 90000, eventhandler.EventHandler{
			Name:    "save configuration",
			Handler: conf.SaveConfiguration,
		})
	}
}

// Configurator provides key-value configuration storage for application.
// There are two types of configuration data - persistent ant temporary.
// Temporary configuration living only while application is working.
// Persistent configuration restored on launch and saved on shutdown.
// Database is used here as backend.
type Configurator struct {
	// Local logger.
	logger zerolog.Logger

	// Persistent configuration that is loaded on startup from database
	// and saved on disk while shutting down.
	configuration      map[string]string
	configurationMutex sync.Mutex

	// Runtime configuration that isn't stored on disk between launches.
	runtime      map[string]interface{}
	runtimeMutex sync.Mutex
}

// GetTempValue returns value for passed key from temporary configuration
// storage. It returns value for passed key if it was found (that still
// HAS to be casted to required type) and nil as error. If key wasn't
// found - it returns empty interface and error.
func (cfg *Configurator) GetTempValue(key string) (interface{}, error) {
	cfg.runtimeMutex.Lock()
	value, valueFound := cfg.runtime[key]
	cfg.runtimeMutex.Unlock()

	if !valueFound {
		cfg.logger.Error().Str("key", key).Msg("Key wasn't found in runtime configuration storage!")
		return nil, errors.New("Key '" + key + "' wasn't found in runtime configuration storage")
	}

	return value, nil
}

// GetValue returns value for passed key from persistent configuration storage.
// It returns value as string and nil as error if key was found and empty
// string and error if key wasn't found.
func (cfg *Configurator) GetValue(key string) (string, error) {
	cfg.configurationMutex.Lock()
	value, valueFound := cfg.configuration[key]
	cfg.configurationMutex.Unlock()

	if !valueFound {
		cfg.logger.Error().Str("key", key).Msg("Key wasn't found in persistent configuration storage!")
		return "", errors.New("Key '" + key + "' wasn't found in persistent configuration storage")
	}

	return value, nil
}

// Initialize initializes Configurator.
func (cfg *Configurator) Initialize(data map[interface{}]interface{}) (bool, error) {
	cfg.logger = c.Logger.With().Str("GUA module", "configurator").Logger()
	cfg.logger.Info().Msg("Initializing configuration manager...")

	cfg.configuration = make(map[string]string)
	cfg.runtime = make(map[string]interface{})

	cfg.initializePaths()

	return true, nil
}

// Initializes required paths, like configuration and cache.
func (cfg *Configurator) initializePaths() {
	cfg.logger.Debug().Msg("Initializing application paths...")

	var path string
	if runtime.GOOS == "windows" {
		homepathWithoutDrive := os.Getenv("HOMEPATH")
		homedrive := os.Getenv("HOMEDRIVE")
		path = filepath.Join(homedrive, homepathWithoutDrive, "AppData", "Roaming", c.GetApplicationName())
	} else if runtime.GOOS == "darwin" {
		homePath := os.Getenv("HOME")
		path = filepath.Join(homePath, "Library", "Application Support", c.GetApplicationName())
	} else {
		homePath := os.Getenv("HOME")
		path = filepath.Join(homePath, ".config", c.GetApplicationName())
	}

	cfg.logger.Debug().Str("OS", runtime.GOOS).Str("path", path).Msg("Detected OS and data path")

	cfg.SetTempValue("/paths/data", path)

	// Create directory if not exist.
	if _, err := os.Stat(path); os.IsNotExist(err) {
		cfg.logger.Debug().Msg("Data path isn't exist, creating directory...")
		os.MkdirAll(path, 0755)
	}
}

// LoadConfiguratio loads configuration from database into persistent
// configuration storage.
func (cfg *Configurator) LoadConfiguration(data map[interface{}]interface{}) (bool, error) {
	cfg.logger.Info().Msg("Loading configuration from local database...")

	cfgData := []models.Configuration{}
	err := c.Database.GetConnection().Select(&cfgData, "SELECT * FROM configuration")
	if err != nil {
		cfg.logger.Fatal().Err(err).Msg("Failed to load configuration from local database")
		return false, errors.New("Failed to load configuration from local database")
	}
	cfg.logger.Debug().Int("items", len(cfgData)).Msg("Got configuration items from database")

	cfg.configurationMutex.Lock()
	for _, data := range cfgData {
		cfg.logger.Debug().Str("key", data.Key).Str("value", data.Value).Msg("Loaded configuration value")
		cfg.configuration[data.Key] = data.Value
	}
	cfg.configurationMutex.Unlock()

	return true, nil
}

// SaveConfiguration saves configuration from persistent storage to
// database.
func (cfg *Configurator) SaveConfiguration(data map[interface{}]interface{}) (bool, error) {
	cfg.logger.Info().Msg("Saving application's configuration...")

	tx, err := c.Database.GetConnection().Beginx()
	if err != nil {
		cfg.logger.Error().Err(err).Msg("Failed to start database transaction, configuration won't be saved!")
		return true, nil
	}

	cfg.configurationMutex.Lock()
	tx.MustExec("DELETE FROM configuration")
	for key, value := range cfg.configuration {
		cfg.logger.Debug().Str("key", key).Str("value", value).Msg("Saving configuration data in database")
		datamodel := models.Configuration{Key: key, Value: value}
		_, err := tx.NamedExec("INSERT INTO configuration (key, value) VALUES (:key, :value)", datamodel)
		if err != nil {
			cfg.logger.Error().Err(err).Msg("Failed to put data in database")
		}
	}
	cfg.configurationMutex.Unlock()

	err1 := tx.Commit()
	if err1 != nil {
		cfg.logger.Error().Err(err1).Msg("Failed to commit transaction to database")
	}
	cfg.logger.Debug().Msg("Configuration saved to database")

	return true, nil
}

// SetTempValue stores passed value for key in temporary configuration
// storage.
func (cfg *Configurator) SetTempValue(key string, value interface{}) {
	cfg.logger.Debug().Str("key", key).Msgf("Storing data in runtime configuration: %+v", value)
	cfg.runtimeMutex.Lock()
	cfg.runtime[key] = value
	cfg.runtimeMutex.Unlock()
}

// SetValue stores passed value for key in persistent configuration
// storage.
func (cfg *Configurator) SetValue(key, value string) {
	cfg.logger.Debug().Str("key", key).Str("value", value).Msg("Storing data in persistent storage")
	cfg.configurationMutex.Lock()
	cfg.configuration[key] = value
	cfg.configurationMutex.Unlock()
}
