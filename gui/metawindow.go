package gui

import (
	// stdlib
	"strconv"

	// other
	"github.com/gotk3/gotk3/gtk"
)

// MetaWindow is a structure that should be embedded in every window
// we create (main window, dialogs, etc.). It contains methods and
// things that makes working with windows easy.
type MetaWindow struct {
	// Passed widget.
	widget interface{}

	// Window's configuration and position.
	winPosX, winPosY, winSizeX, winSizeY int
}

// Initializes application's window.
func (mw *MetaWindow) initializeApplicationWindow(name string, window *gtk.ApplicationWindow) {
	// Size and position.
	c.Logger.Debug().Int("X", mw.winPosX).Int("Y", mw.winPosY).Msg("Moving application's window to new position")

	window.Move(mw.winPosX, mw.winPosY)
	window.SetDefaultSize(mw.winSizeX, mw.winSizeY)

	// Resize and move handler.
	window.Connect("configure-event", func() {
		mw.configureEvent(name)
	})
}

func (mw *MetaWindow) configureEvent(name string) {
	switch mw.widget.(type) {
	case *gtk.ApplicationWindow:
		mw.winSizeX, mw.winSizeY = mw.widget.(*gtk.ApplicationWindow).GetSize()
		mw.winPosX, mw.winPosY = mw.widget.(*gtk.ApplicationWindow).GetPosition()
	case *gtk.Dialog:
		mw.winSizeX, mw.winSizeY = mw.widget.(*gtk.Dialog).GetSize()
		mw.winPosX, mw.winPosY = mw.widget.(*gtk.Dialog).GetPosition()
	default:
		c.Logger.Warn().Msg("Unknown window/widget type, unable to get sizes and positions!")
		return
	}

	c.Logger.Debug().Str("widget", name).Int("size X", mw.winSizeX).Int("size Y", mw.winSizeY).Int("pos X", mw.winPosX).Int("pos Y", mw.winPosY).Msg("Widget info")

	c.Configurator.SetValue("/"+name+"/size_x", strconv.Itoa(mw.winSizeX))
	c.Configurator.SetValue("/"+name+"/size_y", strconv.Itoa(mw.winSizeY))
	c.Configurator.SetValue("/"+name+"/pos_x", strconv.Itoa(mw.winPosX))
	c.Configurator.SetValue("/"+name+"/pos_y", strconv.Itoa(mw.winPosY))
}

// InitializeMetaWindow should be executed in window's initialization
// function.
func (mw *MetaWindow) InitializeMetaWindow(name string, widget interface{}, defaultSizeX, defaultSizeY int) {
	c.Logger.Debug().Msg("Initializing meta things...")

	mw.widget = widget

	// Every widget has a "configure-event" event.
	// Restore window size.
	winSizeXStr, err1 := c.Configurator.GetValue("/" + name + "/size_x")
	if err1 != nil {
		c.Logger.Warn().Msg("Failed to get window's width, will force default value")
		mw.winSizeX = defaultSizeX
	} else {
		winSizeXRaw, err2 := strconv.ParseInt(winSizeXStr, 10, 64)
		if err2 != nil {
			c.Logger.Warn().Msg("Failed to parse window's width, will force default value")
			mw.winSizeX = defaultSizeX
		} else {
			mw.winSizeX = int(winSizeXRaw)
		}
	}
	winSizeYStr, err3 := c.Configurator.GetValue("/" + name + "/size_y")
	if err3 != nil {
		c.Logger.Warn().Msg("Failed to get window's height, will force default value")
		mw.winSizeY = defaultSizeY
	} else {
		winSizeYRaw, err4 := strconv.ParseInt(winSizeYStr, 10, 64)
		if err4 != nil {
			c.Logger.Warn().Msg("Failed to parse window's height, will force default value")
			mw.winSizeY = defaultSizeY
		} else {
			mw.winSizeY = int(winSizeYRaw)
		}
	}

	// Restore window position.
	winPosXStr, err5 := c.Configurator.GetValue("/" + name + "/pos_x")
	if err5 != nil {
		c.Logger.Warn().Msg("Failed to get window's position (X axis), will force default value")
		mw.winPosX = 0
	} else {
		winPosXRaw, err6 := strconv.ParseInt(winPosXStr, 10, 64)
		if err6 != nil {
			c.Logger.Warn().Msg("Failed to parse window's position (X axis), will force default value")
			mw.winPosX = 0
		} else {
			mw.winPosX = int(winPosXRaw)
		}
	}
	winPosYStr, err7 := c.Configurator.GetValue("/" + name + "/pos_y")
	if err7 != nil {
		c.Logger.Warn().Msg("Failed to get window's position (Y axis), will force default value")
		mw.winPosY = 0
	} else {
		winPosYRaw, err8 := strconv.ParseInt(winPosYStr, 10, 64)
		if err8 != nil {
			c.Logger.Warn().Msg("Failed to parse window's position (Y axis), will force default value")
			mw.winPosY = 0
		} else {
			mw.winPosY = int(winPosYRaw)
		}
	}

	// Some window types might require additional setup.
	switch widget.(type) {
	case *gtk.Dialog:
		mw.initializeDialog(name, widget.(*gtk.Dialog))
	case *gtk.ApplicationWindow:
		mw.initializeApplicationWindow(name, widget.(*gtk.ApplicationWindow))
	}
}

// Dialog-specific initialization.
func (mw *MetaWindow) initializeDialog(name string, dialog *gtk.Dialog) {
	// Dialogs should always be modal.
	dialog.SetModal(true)

	// Size and position.
	c.Logger.Debug().Int("X", mw.winPosX).Int("Y", mw.winPosY).Msg("Moving dialog to new positions")

	dialog.Move(mw.winPosX, mw.winPosY)
	dialog.SetDefaultSize(mw.winSizeX, mw.winSizeY)

	// Resize and move handler.
	dialog.Connect("configure-event", func() {
		mw.configureEvent(name)
	})
}
