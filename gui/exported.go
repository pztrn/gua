package gui

import (
	// local
	"gitlab.com/pztrn/gua/context"
)

var (
	c *context.Context
)

func Initialize(cc *context.Context) {
	c = cc
	c.Logger.Debug().Msg("GUI module initialized")
}
