package mainwindow

// MainWindowSettings represents a structure that holds settings for
// application's main window, like window title, to use or not to
// use header bar, and others. Most of these parameters can be
// set only on MainWindow structure initialization.
type MainWindowSettings struct {
	// WindowTitle stores main window title.
	WindowTitle string
	// DefaultWindowSizes stores default window sizes if nothing will
	// be loaded from local database. First element should be width,
	// second element should be height.
	DefaultWindowSizes []int
}
