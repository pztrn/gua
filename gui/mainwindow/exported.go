package mainwindow

import (
	// local
	"gitlab.com/pztrn/gua/context"
)

var (
	c          *context.Context
	mainwindow *MainWindow
)

func NewMainWindow(cc *context.Context) *MainWindow {
	c = cc
	return &MainWindow{}
}
