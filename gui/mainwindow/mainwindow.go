package mainwindow

import (
	// local
	"gitlab.com/pztrn/gua/gui"
)

// MainWindow represents a controlling structure for main window.
// This structure should be embedded in yours.
type MainWindow struct {
	gui.MetaWindow
}
